#!/usr/bin/python3
"""Code to try to build a library of mappings between antiphons, hymns etc and gregobase
score numbers."""
import json
from argparse import ArgumentParser
from urllib.parse import urlencode
from webbrowser import open as open_in_browser

usages = [
    "Alleluia",
    "Antiphona",
    "Canticum",
    "Communio",
    "Graduale",
    "Hymnus",
    "Improperia",
    "Introitus",
    "Kyriale",
    "Offertorium",
    "Toni Communes",
    "Praefationes",
    "Psalmus",
    "Responsorium brevis",
    "Responsorium",
    "Sequentia",
    "Tractus",
    "Varia",
]

library = []
library_loaded = False


def prompt(message, y="y", n="n"):
    """y = yes, n = no, q = return 'none' (for quit), x=raise
    StopIteration (for cases where we need to be able _really_ to quit)

    Parameters
    ----------
    message :
        
    y :
        (Default value = "y")
    n :
        (Default value = "n")

    Returns
    -------

    
    """
    y = y.lower()
    n = n.lower()
    while 1:
        choice = input(message + f" {y}/{n} ").lower()
        if choice in [y, n]:
            break
        else:
            print("Illegal input, try again")
    if choice == y:
        return True
    else:
        return False


def generate_usage_url(usage: str):
    """

    Parameters
    ----------
    usage: str : Usage category as a string.
        

    Returns
    -------

    
    """
    if usage == "Responsorium brevis":
        category_id = "rb"
    else:
        category_id = usage[0:2].lower()
        base_url = "https://gregobase.selapa.net/usage.php?"
    return base_url + urlencode({"id": category_id})


def generate_url(score_id: str):
    """

    Parameters
    ----------
    score_id : str: Gregobase score id (an int, but a str)
        

    Returns
    -------

    
    """
    base_url = "https://gregobase.selapa.net/chant.php?"
    return base_url + urlencode({"id": score_id})


def load_library():
    """Load library."""
    global library, library_loaded
    """Load local library (just json: why make it complicated?)"""
    try:
        with open("gregobase/library.json", "r") as f:
            library = json.load(f)
        library_loaded = True
    except json.decoder.JSONDecodeError:
        print(
            "!!Warning: error decoding, library is blank",
            "(you should check library.json NOW)",
        )


def save_library():
    """Save library."""
    global library, library_loaded
    if not library_loaded:
        if not prompt("Library not loaded successfully.  Overwrite?"):
            return

    with open("gregobase/library.json", "w") as f:
        f.write(json.dumps(library, indent=2))


def prompt_list(prompts: list):
    """
    Enumerate a list and then make the user choose one.

    Parameters
    ----------
    prompts : list : a list of prompts (strings)



    Returns
    -------
    Index (int) of chosen element in prompts
    """
    for index, prompt in enumerate(prompts):
        print(f"({index}) {prompt}")
    choice = input("Selection? ")
    while choice not in range(len(prompts)):
        print("Invalid input, try again.")
        choice = input("Selection ?")

    return choice


def search_gregobase(incipit: str, usage: str):
    """
    Open webbrowser so we can find the id manually.

    Parameters
    ----------
    incipit: str : Incipit of chant

    usage: str : Usage of chant as specified on gregobase


    Returns
    -------
    Score ID as str (manually entered by user)
    """
    incipit = incipit.lower()
    try:
        usage = usage.title()
    except AttributeError:
        pass
    if usage not in usages:
        print(f"!!Warning: passed usage {usage} not in recorded usages, ignoring")
        usage = None
    print(incipit)
    open_in_browser(generate_usage_url(usage))
    score_id = input("Score ID? ")
    festival = input("Festival?  (for none just press enter)")
    make_record(incipit, usage, score_id, festival)
    save_library()
    return score_id


def search_library(incipit, usage, festival):
    """
    Search local library for records matching the given incipit, usage (and anything
    else I think of later)

    Parameters
    ----------
    incipit : incipit

    usage : usage

    Returns
    -------
    gregobase score id
    """
    usage = usage.title()
    incipit = incipit.lower()
    festival = festival.title()
    if usage not in usages:
        print(f"!!Warning: passed usage {usage} not in recorded usages, ignoring")
        usage = None

    found = []
    global library
    for record in library:
        if record["incipit"] == incipit:
            if usage:
                if record["usage"] != usage:
                    print(
                        f"Continuing due to usage mismatch between",
                        "{record['usage']} and {usage}.",
                    )
                    continue
            if festival:
                if record["festival"] != festival:
                    print(
                        f"Possible error in posing {record['festival']} for {festival}."
                    )
            found.append(record)

    if not found:
        return search_gregobase(incipit, usage)
    elif len(found) == 1:
        return found[0]["score_id"]
    else:
        # need to guess which one
        print(f"Found {len(found)} records; opening in web browser")
        for record in found:
            open_in_browser(generate_url(record["score_id"]))
        choice = prompt_list([record["score_id"] for record in found])
        return choice


def make_record(incipit, usage, score_id, festival):
    """
    Make record in library.

    Can created duplicates.
    """
    library.append(
        {"incipit": incipit, "usage": usage, "score_id": score_id, "festival": festival}
    )


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("INCIPIT")
    parser.add_argument("USAGE", help=f"one of {usages}")
    parser.add_argument("--ignore_local", action="store_true")
    args = parser.parse_args()
    load_library()
    if args.ignore_local:
        print(search_gregobase(args.INCIPIT, args.USAGE))
    else:
        print(search_library(args.INCIPIT, args.USAGE))
    save_library()
