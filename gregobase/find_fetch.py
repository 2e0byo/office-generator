#!/usr/bin/python3
"""Code to locate an antiphon or hymn (for now) on
gregobase.selapa.net and download it"""

import re
from time import sleep
from urllib.parse import urlencode

import requests

from gregobase.library import generate_url
from util import objects

base_url = "https://gregobase.selapa.net/download.php?"


def safe_fetch(url: str):
    """Fetch url with timeout and retries, handling errors with
    exponential backoff of up to 5s.  Ulimately, raise RequestException.

    Parameters
    ----------
    url : str : URL to fetch.

    """
    attempts = 0
    while attempts < 3:
        try:
            r = requests.get(url, timeout=3)
            r.raise_for_status()
            return (r)
        except requests.exceptions.RequestException as e:
            print(e)
            attempts += 1
            delay = attempts * 0.5
            print("waiting %s s for reconnect" % delay)
            sleep(delay)  # exponential backoff
    raise requests.exceptions.RequestException


def download_score(score_id: str):
    """Download score with given score_id

    Parameters
    ----------
    score_id: str :
        

    Returns
    -------

    """
    url = base_url + urlencode({"id": score_id, "format": "gabc"})
    return safe_fetch(url)


def parse_score(score: str, score_id: str, force_termination=False):
    """Parse a score text into a score object

    Parameters
    ----------
    score: str :
        
    score_id: str :
        
    force_termination :
         (Default value = False)

    Returns
    -------

    """
    name = re.findall(".*name:(.*);.*", score)[0]
    mode = re.findall(".*mode:(.*);.*", score)[0]
    if len(mode) > 1:
        termination = mode[1:]
        mode = mode[:1]
    else:
        if mode in ["2", "5", "6", "per"]:
            termination = None
        elif not force_termination:
            termination = None
        else:
            print(f"Manually enter termination for {name} at " +
                  "{generate_url(score_id)}")
            termination = input("Termination: ")
    try:
        book = re.findall(".*book:(.*);.*", score)[0]
    except IndexError:
        book = None
    try:
        office_part = re.findall(".*office-part:(.*);.*", score)[0]
    except IndexError:
        office_part = None

    return objects.score(name, mode, score, termination, book, office_part,
                         score_id)


if __name__ == "__main__":
    gabc = download_score(12)
    print(gabc.text)
