#!/usr/bin/python3
import shutil
from os import path
from pathlib import Path

import jinja2

import psalmify
from gregobase import find_fetch, library
from util import (
    document as document_object,
    feast as feast_object,
    score as score_object,
)

latex_jinja_env = jinja2.Environment(
    block_start_string="\BLOCK{",
    block_end_string="}",
    variable_start_string="\VAR{",
    variable_end_string="}",
    comment_start_string="\#{",
    comment_end_string="}",
    line_statement_prefix="%%",
    line_comment_prefix="%#",
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(path.abspath("templates/")),
)


def get_incipit(season: str, style: str = "simple"):
    """
    Get appropriate incipit for office.  Curently just returns the same one every time.

    Parameters
    ----------

    season: str :

    style: str : (Default value = "simple")

    Returns
    -------
    """
    with open("./gabc/incipit.gabc") as f:
        return score_object(f.read())


def fetch_gregorian(incipit: str, usage: str, feast: feast_object = None):
    """
    Fetch gregorian score by incipit and usage.

    Parameters
    ----------
    incipit: str : Incipit of score

    usage: str : Usage (office part)

    feast: feast_object : What is this feast of, anyway?  Currently merely advisory.


    Returns
    -------

    gabc (which we generally need to write somewhere) as a score object
    """
    score_id = library.search_library(incipit, usage, feast.name)
    gabc = find_fetch.download_score(score_id)
    return score_object(gabc.text)


def generate_major_office(dirname: Path, office: dict, style: str = "Rubrics 1960"):
    """

    Parameters
    ----------
    dirname: Path : Directory in which to write output.
        
    office: dict : Parameters for the office when generated.
        
    style:str : Style of office, options are: 'Trident 1570', 'Trident 1910', 
                'Divino Afflatu', 'Reduced 1955',    'Rubrics 1960','1960 Newcalendar'
         (Default value = "Rubrics 1960")

    Returns
    -------
    dirname : Path : output dir

    filename : str : output filename

    """
    if not dirname.is_dir():
        if dirname.exists():
            raise FileExistsError(f"Output dir {str(dirname)} is a file!")
        else:
            dirname.mkdir()

    # Assemble target dir
    shutil.copy("./templates/preamble.tex", dirname)
    shutil.copy("./templates/rubrics.sty", dirname)

    for filename, part in (
        ("incipit.gabc", "incipit"),
        ("hymn.gabc", "hymn"),
        ("canticle_ant.gabc", "canticle_ant"),
        ("canticle.gabc", "canticle_gabc"),
    ):
        full_path = dirname / filename
        with full_path.open("w") as f:
            f.write(office[part].gabc)

    for psalm in office["psalms"]:
        full_path = dirname / (psalm["score"] + ".gabc")
        with full_path.open("w") as f:
            f.write(psalm["score_gabc"].gabc)

        full_path = dirname / (psalm["ant"] + ".gabc")
        with full_path.open("w") as f:
            f.write(psalm["ant_gabc"].gabc)

    template = latex_jinja_env.get_template(path.join(style, "major.tex"))
    filename = office["office"] + ".tex"

    full_path = dirname / filename
    with full_path.open("w") as f:
        f.write(template.render(office))

    return dirname, filename


def assemble_office(office: dict):
    """
    Get all the relevant gabc/tex bits together.

    Parameters
    ----------

    office: dict : parameters for the office.  These are identically
    named with their later replacements, but *only strings* as we do
    the work here.

    Returns
    -------
    dict, formatted for generate_*_office
    """

    # get antiphons and process psalms
    i = 1
    for psalm in office["psalms"]:
        # get antiphon, as this gives us tone and termination
        psalm["ant_gabc"] = fetch_gregorian(psalm["ant"], "Antiphona", office["feast"])
        psalm["ant"] = f"psalm{str(i)}ant"
        mode = psalm["ant_gabc"].mode()
        termination = psalm["ant_gabc"].termination()
        psalm["score_gabc"], psalm["psalm"] = psalmify.psalm(
            psalm["psalm"], mode, termination, solemn=False
        )
        psalm["score"] = f"psalm{str(i)}"
        i += 1

    # get hymn
    office["hymn"] = fetch_gregorian(office["hymn"], "Hymnus", office["feast"])
    if office["canticle"]:
        # get canticle ant
        office["canticle_ant"] = fetch_gregorian(
            office["canticle_ant"], "Antiphona", office["feast"]
        )

        # get canticle
        mode = office["canticle_ant"].mode()
        termination = office["canticle_ant"].termination()
        if str(office["feast"]) == "major":
            solemn = True
        else:
            solemn = False
        office["canticle_gabc"], office["canticle"] = psalmify.psalm(
            office["canticle"], mode, termination, solemn
        )

    return office


if __name__ == "__main__":
    library.load_library()
    titlepage_template = latex_jinja_env.get_template("booklet_titlepage.tex")

    template = latex_jinja_env.get_template("Divino Afflatu/major.tex")
    office = {}
    office["titlepage"] = titlepage_template.render(
        header="Divinum Officium", title="Lauds", subtitle="Nonesuch Day"
    )
    office["feast"] = feast_object("SS. Apostolorum Petri et Pauli")
    office["feast"].set_rank("i class")
    office["office"] = "Vespers"
    office["psalms"] = [
        {"ant": "Petrus et Joannes", "psalm": "109"},
        {"ant": "Argentum et aurum", "psalm": "110"},
        {"ant": "Dixit Angelus ad Petrum", "psalm": "111"},
        {"ant": "Misit Dominus", "psalm": "112"},
        {"ant": "Tu es Petrus", "psalm": 116},
    ]
    office[
        "chapter"
    ] = "Misit Heródes rex manus ut afflígeret quosdam de Ecclésia. Occídit autem Jacóbum fratrem Joánnis gládio. Videns autem quia placéret Judǽis, appósuit ut apprehénderet et Petrum."
    office["hymn"] = "Decora lux aeternitatis"
    office[
        "hymn_translation"
    ] = """
The beauteous light of God's eternal majesty\\
Streams down in golden rays to grace this holy day\\
Which crowned the princes of the Apostles' glorious choir,\\
And unto guilty mortals showed the heavenward way.

The teacher of the world and keeper of heaven's gate,\\
Rome's founders twain and rulers too of every land,\\
Triumphant over death by sword and shameful cross,\\
With laurel crowned are gathered to the eternal band.

O happy Rome! who in thy martyr princes' blood,\\
A twofold stream, art washed and doubly sanctified.\\
All earthly beauty thou alone outshinest far,\\
Empurpled by their outpoured life-blood's glorious tide.

All honour, power, and everlasting jubilee\\
To him who all things made and governs here below,\\
To God in essence One, and yet in persons Three,\\
Both now and ever, while unending ages flow. Amen.
"""
    office[
        "verse"
    ] = """
    \\l{\\v~In omnem terram exívit sonus eórum.}
    \\e{}
    \\l{\\r~Et in fines orbis terræ verba eórum}
    \\e{}
"""
    office["canticle_ant"] = "Tu es pastor óvium"
    office[
        "canticle_ant_translation"
    ] = "Thou art the Shepherd * of the sheep, and the Prince of the Apostles, and unto thee are given the keys of the kingdom of heaven."
    office["canticle"] = "Magnificat"
    office["canticle_gabc"] = score_object("Canticle", "mode", "gabc")
    office[
        "collect"
    ] = """
\\l{Deus, qui hodiérnam diem Apostolórum tuórum Petri et Pauli martýrio consecrásti: da Ecclésiæ tuæ, eórum in ómnibus sequi præcéptum; per quos religiónis sumpsit exórdium.}
\\e{O God, Who didst hallow this day by the Testifying of thine Holy Apostles Peter and Paul, grant unto thy Church, whose foundations Thou wast pleased to lay by their hands, the grace always in all things to remain faithful to their teaching.}
"""
    office[
        "commemorations"
    ] = """
\\red{Commemoratio Dominica IV Post Pentecosten}
\\ant Præcéptor, per totam noctem laborántes nihil cépimus; in verbo autem tuo laxábo rete.

\\v~Dirigátur, Dómine, orátio mea.\\
\\r~Sicut incénsum in conspéctu tuo.

\\black{
\\oremus
\\l{Da nobis, quǽsumus, Dómine: ut et mundi cursus pacífice nobis tuo órdine dirigátur; et Ecclésia tua tranquílla devotióne lætétur.}
\\e{Grant, O Lord, we beseech thee, that the course of this world may be so peaceably ordered by thy governance, that thy Church may joyfully serve thee in all godly quietness.}
\\amen}
"""
    office["incipit"] = get_incipit("per annum")
    office = assemble_office(office)

    dirname, filename = generate_major_office(
        Path("Vespers_SS._Petri_et_Pauli"), office, style="Divino Afflatu"
    )
    vespers = document_object(dirname, filename)
    vespers.compile()
