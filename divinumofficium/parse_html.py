"""Parse divinumofficium downloads."""

from bs4 import BeautifulSoup

from util import feast as feast_object, versus as versus_object


class DivinumOfficiumParseError(Exception):
    pass


def parse_psalm_soup(soup: BeautifulSoup):
    """
    Parse psalm soup.

    Parameters
    ----------
    soup: BeautifulSoup : soup to parse


    Returns
    -------
    antiphon : str, psalms: list(str), psalm_name : str
    """
    started = False
    ant = ""
    verses = []
    psalm_name = None
    for font in [
            i for i in soup.find_all("font", color="red")
            if i.get("size") != "+1"
    ]:
        if not started:
            if font.i:
                started = True
                for sibling in font.next_siblings:
                    if str(sibling) == "<br/>":
                        break
                    else:
                        ant += str(sibling)
                continue
            else:
                continue

        # verses
        if not psalm_name:
            psalm_name = font.text.replace("Psalmus ", "")
        verse = ""
        for sibling in font.next_siblings:
            try:
                if sibling.get("color") == "black":
                    continue
            except AttributeError:
                pass
            if str(sibling) == "<br/>":
                break
            else:
                verse += str(sibling)
        if verse:
            verses.append(verse.strip())
    return ant, verses, psalm_name


def get_br(br: BeautifulSoup, text=True):
    html = []
    for tag in br.next_siblings:
        if tag.name == "br":
            break
        else:
            if text:
                try:
                    html.append(tag.text)
                except AttributeError:
                    html.append(tag)
            else:
                html.append(tag)
    return html


def parse_html(html: str):
    office = {}

    soup = BeautifulSoup(html)
    s = soup.find_all("select", id="version")[0]
    for option in s.find_all("option"):
        try:
            option["selected"]
            office["version"] = option["value"]
        except KeyError:
            pass
    s = soup.find_all("select", id="lang2")[0]
    for option in s.find_all("option"):
        try:
            option["selected"]
            office["lang2"] = option["value"]
        except KeyError:
            pass

    office["office"] = soup.title.text
    feast_name, rank_string = soup.find_all(
        "p", align="CENTER")[0].font.text.split(" ~ ")
    office["feast"] = feast_object(feast_name)
    office["feast"].set_rank(rank_string)

    # new approach: let's go through the whole thing.
    tds = soup.find_all("td")  # sections

    # find incipit
    incipit = False
    for td in tds:
        if td.find_all(string="Incipit"):
            incipit = td
            break
    if not incipit:
        raise DivinumOfficiumParseError("Couldn't find incipit")

    for s, key in ((td, "incipit"), (tds[tds.index(td) + 1],
                                     "incipit_translation")):
        red_italic = [x for x in s.find_all("font", color="red") if x.i]

        started = False
        incipit = []
        for r in red_italic:
            if not started:
                if r.text == "V.":
                    started = True
                else:
                    continue
            text_string = r.text
            for sibling in r.next_siblings:
                if str(sibling) == "<br/>":
                    break
                else:
                    try:
                        text_string += sibling.text
                    except AttributeError:
                        text_string += sibling
            incipit.append(text_string)

            office[key] = incipit

    # find psalm
    psalms = []
    translations = []
    for i, td in enumerate(tds):
        if td.find_all(string="Psalmi"):

            # find end of section
            end = 0
            for end in range(i + 2, len(tds)):
                if tds[end].find_all("font", color="red", size="+1"):
                    break
            for latin_index in range(i, end, 2):
                psalms.append(tds[latin_index])
            for vernacular_index in range(i + 1, end, 2):
                translations.append(tds[vernacular_index])
            break
    if not psalms or not translations:
        raise DivinumOfficiumParseError("Unable to parse psalms/translations")

    office["psalms"] = []
    for psalm in psalms:
        ant, verses, psalm_name = parse_psalm_soup(psalm)
        office["psalms"].append({
            "ant": ant,
            "verses": verses,
            "psalm": psalm_name
        })

    office["psalm_translations"] = []
    for psalm in translations:
        ant, verses, psalm_name = parse_psalm_soup(psalm)
        office["psalm_translations"].append({
            "ant": ant,
            "verses": verses,
            "psalm": psalm_name
        })

    # find hymn
    hymn_soup = None
    translation_soup = None
    for td in tds:
        if td.find_all("font", string="Hymnus"):
            hymn_soup = td
            continue
        if hymn_soup:
            translation_soup = td
            break
    if not translation_soup or not hymn_soup:
        raise DivinumOfficiumParseError("Unable to parse hymn or translation")

    hymn_index = None
    for i, br in enumerate(hymn_soup.find_all("br")):
        html = get_br(br)
        if "Hymnus" in html:
            hymn_index = i + 1
            break

    hymn = []
    verse = []
    started = False
    for br in hymn_soup.find_all("br")[hymn_index:]:
        if started:
            if br.next_sibling.next_sibling.name == "font":
                break

        started = True
        line = "".join(get_br(br)).strip()
        if not line:
            hymn.append("\\\\\n".join(verse))
            verse = []
        else:
            verse.append(line)
    office["hymn"] = hymn

    hymn = []
    verse = []
    started = False
    for br in translation_soup.find_all("br")[hymn_index:]:
        if started:
            if br.next_sibling.next_sibling.name == "font":
                break

        started = True
        line = "".join(get_br(br)).strip()
        if not line:
            hymn.append("\\\\\n".join(verse))
            verse = []
        else:
            verse.append(line)
    office["translation"] = hymn

    # chapter

    chapter_soup = None
    translation_soup = None
    for td in tds:
        if td.find_all("font",
                       string=lambda string: string and "Capitulum" in string):
            chapter_soup = td
            continue
        if chapter_soup:
            translation_soup = td
            break
    if not chapter_soup or not translation_soup:
        raise DivinumOfficiumParseError(
            "Unable to find chapter or translation")

    ref = ""
    chapter = []
    versus = []
    started = False
    brs = chapter_soup.find_all("br")
    ref = "".join(get_br(brs[0])).strip()
    chapter = "".join(get_br(brs[1])).strip()
    office["chapter_ref"] = ref
    office["chapter"] = chapter

    brs = translation_soup.find_all("br")
    ref = "".join(get_br(brs[0])).strip()
    chapter = "".join(get_br(brs[1])).strip()
    office["chapter_ref_translation"] = ref
    office["chapter_translation"] = chapter

    for br in chapter_soup.find_all("br")[3:]:
        line = get_br(br)
        if "V." in line:
            versus = line[2].strip()
            continue
        if versus:
            responsorium = line[2].strip()
            break

    versus_translation = None
    for br in translation_soup.find_all("br")[3:]:
        line = get_br(br)
        if "V." in line:
            versus_translation = line[2].strip()
            continue
        if versus_translation:
            responsorium_translation = line[2].strip()
            break

    office["verse"] = versus_object(versus, responsorium, versus_translation,
                                    responsorium_translation)

    if office["office"].replace("Divinum Officium ",
                                "") in ["Laudes", "Vesperae", "Completorium"]:
        canticum_soup = None
        translation_soup = None
        for td in tds:
            if canticum_soup:
                translation_soup = td
                break
            if td.find_all(
                    "font",
                    string=lambda string: string and "Canticum" in string):
                canticum_soup = td
                continue
        if not canticum_soup or not translation_soup:
            raise DivinumOfficiumParseError(
                "Unable to parse canticum or translation")

        office["canticle"] = canticum_soup.find_all(
            "font", string=lambda string: string and "Canticum" in string
        )[0].text.replace("Canticum ", "").strip()
        office["canticle_ant"] = canticum_soup.find_all(
            "font", string="Ant.")[0].next_sibling

        office["canticle_ant_translation"] = translation_soup.find_all(
            "font", string="Ant.")[0].next_sibling

        started = False
        verse = []
        for br in translation_soup.find_all("br")[2:]:
            if started:
                if "Ant." in get_br(br):
                    break

            started = True
            line = "".join(get_br(br)[2:]).strip()
            verse.append(line)
        office["canticle_translation"] = "\\\\\n".join(verse)
    return office


if __name__ == "__main__":

    with open("divinumofficium/Divinum Officium Vesperae.html") as f:
        a = parse_html(f.read())
        print(a.keys())
