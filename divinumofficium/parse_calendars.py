#!/usr/bin/python3
"""Code to parse divinum_officium's calendars, which are not exactly
straightforward."""

from os import path

divinum_officium_path = ""      # this should be defined after importing!
horas_path = "web/www/horas/"

# look in all the relevant files, then apply the right algorithm to work out what the final amalgam is
