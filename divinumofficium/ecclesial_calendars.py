"""We don't just call this 'calendar' to avoid shadowing."""


class Calendar:
    """Base class for calendar objects."""

    def __init__(self, name):
        self.name = name

    def precedence(self, candidates: list):
        """
        Calculate precedence of any given day.

        A subclass is expected to implement this.
        """


class Calendar_1962(Calendar):
    """Object to represent the 1962 calendar, with its much simplified precedence
    rules."""

    def precedence(self, candidates: list):
        return max(
            candidates
        )  # this can't be right.  There still /are/ commemorations in the '62.
