"""Config variables imported into everything."""


"""0 = no automation, 3 = full"""
automation_level = 3

"""List containing command to be run to compile.  Will be run twice, in output dir."""
compile_command = ["lualatex", "--shell-escape"]
