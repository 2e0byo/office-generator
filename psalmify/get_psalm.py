"""Get psalm from already formatted psalms in a dir (easier for now...)"""

from os import path

psalms_dir = path.expanduser(
    "~/Library/Music/St-Cuthberts-Liturgy/gregorian-psalms/psalms"
)


def psalm(psalm: str, tone: str, termination: str, solemn: bool = False):
    """
    Get a psalm from the psalms_dir.  If it's not there, oops..

    Parameters
    ----------
    psalm: str : Psalm or canticle, exactly as appearing in psalms_dir

    tone: str : tone

    termination: str : termination, if any.  If tone is 2 or per, ignored.

    solemn: bool : Solemn tone or not.
         (Default value = False)

    Returns
    -------
    Psalm gabc, and psalm tex.
    """
    from util import score as score_object

    if tone in ["2", "per"]:
        termination = ""
    if not termination:
        termination = ""
    if solemn:
        tone = f"solemn{tone}"

    prefix = f"{psalm}-{tone}{termination}"
    with open(path.join(psalms_dir, f"{prefix}.gabc"), "r") as f:
        gabc = score_object(f.read())
    with open(path.join(psalms_dir, f"{prefix}.tex"), "r") as f:
        tex = f.read()

    return gabc, tex
