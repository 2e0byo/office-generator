"""
Code to point psalms for singing and set to tones.

This is all ported from javascript source at
http://bbloomf.github.io/jgabc
"""

from .get_psalm import *
