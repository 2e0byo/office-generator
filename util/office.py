"""Classes to represent offices."""
from dataclasses import dataclass
from os import path

divinum_officium_path = path.expanduser("~/code/divinum-officium")
horas_path = path.join(divinum_officium_path, "/web/www/horas")

language_paths = {
    "francais": path.join(horas_path, "Francais"),
    "english": path.join(horas_path, "English"),
}

translations = {}
latin = {}


@dataclass
class text_object:
    """Class to represent text with zero or more other properties."""

    text: str
    name: str
    sung: bool = False
    translation: str = False
    lang: str = "Latin"


def get_translations(lang: str):
    """Parse Translate.txt in relevant Psalterium folder and return dict."""
    # open relevant lang file
    translations = {}
    k = ""
    v = ""
    with open(path.join(language_paths[lang], "Psalterium/Translate.txt"), "r") as f:
        for line in f.read():
            if line.startswith("["):
                k = line.strip().strip("[]")
            elif k:
                v = text_object(line.strip(), k, lang=lang)
            else:
                translations[k] = v
                k, v = "", ""
    return translations


def init(lang: str):
    """Parse all the stuff we need to start work."""
    global latin, translations
    latin = get_translations(
        "Latin"
    )  # eventually we will want to add formatting step here for latin only, or more likely, just replace the file with a pre-formatted one in the same style
    if lang:
        translations = get_translations(lang)


def get_translation(obj: text_object):
    """Get appropriate translation for a string and format it as a text_object."""
    return text_object(translations[obj.name], obj.name)


@dataclass
class feast:
    """Class to represent the rank, etc of every day."""

    name: str
    rank: int = 0

    def set_rank(self, rank):
        """Set a rank int a tad easier."""
        rank = rank.lower()
        if rank in ["feria"]:
            self.rank = 0
        if any(
            [x in rank for x in ["simple", "iii class", "3rd class", "iii. classis"]]
        ):
            self.rank = 1
        if any(
            [
                x in rank
                for x in [
                    "double of the ii class",
                    "ii class",
                    "2nd class",
                    "ii. classis",
                ]
            ]
        ):
            self.rank = 2
        if any(
            [
                x in rank
                for x in ["double of the i class", "i class", "1st class", "i. classis"]
            ]
        ):
            self.rank = 3

    def __str__(self):
        """Render in a format suitable for library lookups."""
        if self.rank == 0:
            return "feria"
        elif self.rank < 3:
            return "minor"
        else:
            return "major"


# @dataclass
# class office:
#     psalmi: list
#     capitulum: text_object
#     hymnus: text_object
#     versus: versus
#     oratio: text_object
#     suffragium: text_object
#     ordo: str = "1960"
#     preces_feriales: bool = False
#     suppress_incipit: bool = False  # false during the triduum
#     sacerdos: bool = False
#     alleluia: bool = True
#     second_language: str = None

#     def __post_init__(self):
#         """Set up translations."""
#         self.latin = deepcopy(latin)
#         if not self.second_language:
#             return
#         else:
#             for k, v in iter(self.latin):
#                 v.translation = translations[k]
#         # Now we can mess with our local version e.g. replacing with snippet to insert gregorian chant without worrying

#     def incipit(self):
#         """Render incipit if appropriate."""
#         if self.suppress_incipit:
#             return None

#         incipit = []

#         if self.ordo in ["1570", "1910", "divino_afflatu"]:
#             incipit += [latin["Pater noster"], latin["Ave Maria"]]

#         incipit += [latin["Deus in adjutorium"], latin["Gloria"]]
#         if self.alleluia:
#             incipit += latin["Alleluia"]
#         else:
#             incipit += latin["Laus tibi"]

#         # elif self.ordo == "Monasticum":
#         #     return "Psalm 66 Here!"

#     def conclusio(self):
#         """Render conclusion as appropriate."""


# class major_office(office):
#     """Class to represent Lauds and Vespers."""


# class mattins(office):
#     """Class to represent Mattins."""


# class minor_office(office):
#     """Class to represent Prime, Terce, Sext or None."""
