#!/usr/bin/python3
"""
Utility modules and object definitions.

This init currently does nothing.
"""

from .objects import *
from .office import *
from .ordinarium_parser import *
