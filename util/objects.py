"""Objects of varying kinds to represent things short of full offices."""
import re
from dataclasses import dataclass
from os import chdir
from pathlib import Path
from subprocess import CalledProcessError, run

import config
from psalmify import tones
from templates import load_template

jinja_env = load_template.latex_jinja_env


@dataclass
class score:
    """Class to represent a gregorian score."""

    gabc: str
    _name: str = None
    _mode: str = None
    _termination: str = None
    book: str = None
    office_part: str = None
    gregobase_score_id: str = None

    def name(self):
        """Get and return name."""
        if self._name:
            return self._name
        else:
            self._name = re.findall(".*name:(.*);.*", self.gabc)[0]
        return self._name

    def mode(self):
        """Get and return mode."""
        if self._mode:
            return self._mode

        try:
            mode = re.findall(".*mode:(.*);.*", self.gabc)[0]

            if len(mode) > 1:
                try:
                    self._mode, self._termination = re.findall("([0-9])(.*)", mode)
                except IndexError:
                    self._mode = mode.lower().strip()
            else:
                self._mode = mode
        except IndexError:
            print(f"Unable to find mode in gabc ({self.gabc[:100]})")
            self._mode = input(f"Please enter mode manually: ")

        return self._mode

    def termination(self, spell_out=True, force=False):
        """
        Get and return termination.

        Parameters
        ----------
        spell_out : return 'star' not '*'
             (Default value = True)
        force : force (prompt) a termination if not present and automation = 0
             (Default value = False)

        Returns
        -------
        termination
        """
        if not self._mode:
            self.mode()

        if self._mode in ["2", "5", "6", "per"]:
            self._termination = None
        elif not self._termination:
            if config.automation_level > 0:
                self._termination = list(
                    tones.g_tones[self._mode]["terminations"].keys()
                )[0]
            elif force:
                print(f"Manually enter termination for {self.name()}")
                self._termination = input("Termination: ")

        if spell_out:
            self._termination.replace("*", "star")
        else:
            self._termination.replace("star", "*")

        return self._termination


@dataclass
class document:
    """Class to represent document."""

    out_dir: Path
    filename: str

    def compile(self):
        """"""
        wdir = Path.cwd()
        chdir(self.out_dir)
        try:
            for i in range(2):
                print(f"Compiling {self.filename}, run {i} of 2...")
                run(config.compile_command + [self.filename], check=True)
        except CalledProcessError:
            print(f"compilation failed for {self.filename} in {str(self.out_dir)}")
        chdir(wdir)


@dataclass
class versus:
    """Class to represent versus."""

    versus: str
    responsorium: str
    versus_translation: str
    responsorium_translation: str

    def __str__(self):
        template = jinja_env.get_template("versus.tex")
        latex = template.render(
            {
                "versus": self.versus,
                "versus_translation": self.versus_translation,
                "responsorium": self.responsorium,
                "responsorium_translation": self.responsorium_translation,
            }
        )
        return latex
